---
layout: Downloads
title: Downloads
metaTitle: Downloads
description: Download page for the latest versions of Aurora Apps.
meta:
    - name: keywords
      content: Download, Official, Android app, App Store, APK
lang: de
---

# Downloads

Download our latest apps here.

## <img class="headerLogo" :src="$withBase('/icons/aurora_store.png')"> [Aurora Store](/de/download/AuroraStore.md)

The latest version of Aurora Store was uploaded <ReleaseDateStore stable /> and released **7 months ago**.

Note: all current nightly builds have been paused until further notice.

<DownloadButtonsStore />

<ChangelogStore />

## <img class="headerLogo" :src="$withBase('/icons/aurora_droid.png')"> [Aurora Droid](/de/download/AuroraDroid.md)

The latest version of Aurora Droid was uploaded <ReleaseDateDroid stable /> and released **6 months ago**.

<DownloadButtonsDroid />

<ChangelogDroid />

## <img class="headerLogo" :src="$withBase('/icons/app_warden.png')"> [App Warden](/de/download/AppWarden.md)

The latest version of App Warden was released <ReleaseDateWarden stable />.

<DownloadButtonsWarden />

<ChangelogWarden />

## <img class="headerLogo" :src="$withBase('/icons/aurora_wallpapers.png')"> [Aurora Wallpapers](/de/download/AuroraWallpapers.md)

The latest version of Aurora Wallpapers was released <ReleaseDateWalls stable />

<DownloadButtonsWalls />

<ChangelogWalls />
