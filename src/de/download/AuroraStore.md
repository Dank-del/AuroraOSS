---
layout: DownloadPage
title: Aurora Store
metaTitle: Aurora Store
description: An open source alternative Google Play Store frontend with privacy features and clean UI.
lang: de
meta:
  - property: og:image
    content: /icons/aurora_store.png
  - name: theme-color
    content: "#38CA79"
  - name: msapplication-TileColor
    content: "#38CA79"
sidebar: false
---

# <img class="headerLogo" :src="$withBase('/icons/aurora_store.png')"> Aurora Store

## About

Aurora Store is an open-source alternative Google Play Store frontend client with privacy and modern design in mind. Not only does Aurora Store download, update, and search for apps like the Play Store, it also empowers the user with useful features.

## Screenshots

<img class="zoomable" :src="$withBase('/assets/screenshots_store.png')"/>

## Features:

- **Anonymous login**: Login anonymously so that you stay private.
- **Google login**: access your paid apps and install beta apps.
- **Device spoofing**: App not available for your device? Use available device configurations to spoof you device!
- **Location spoofing**: With the help of location spoofing and a VPN, you can access gloabally restricted apps!
- **Filter F-Droid app**: Don't want F-Droid apps to be in your update list? Use the filter to exclude them.
- **Blacklist apps**: By blacklisting apps, Google will not know that the selected apps are installed on your device.

## Credits

- [whyorean](https://gitlab.com/whyorean/) (Rahul Patel) for making the app.
- [IacobIonut01](https://gitlab.com/IacobIonut01/) for the sweet-looking UI.

## Known Bugs

::: guide Bugs 
- **Categories** tab shows "No apps blacklisted" [WIP]
:::

## Disclaimer

The developer of this application is not responsible for getting your Google account banned, device malware or anything affiliated with apps in the Google Play Store.

As per [**Google Play Terms of Service §4**](https://play.google.com/intl/en-us_us/about/play-terms/index.html), under "Restrictions" there are a number of things that could potentially be used against you for the deactivation of your personal Google account that you want to remain using.

## Support Group

#### Telegram

<p align="center">
	<img class="zoomable" :src="$withBase('/assets/tg-aurorasupport-qr.png')" width="175px"/>
</p>