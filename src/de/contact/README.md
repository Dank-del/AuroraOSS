---
layout: Contact
title: AuroraOSS contact
metaTitle: AuroraOSS contact
description: Contact the AuroraOSS Team!
lang: de
meta:
  - property: og:image
    content: /icons/auroralogo.png
sidebar: true
---
# Contact

## The Team

<ContactsPage />

## Support

Any questions our FAQs can't answer, problems that you want to discuss discreetly or trust issues with our apps? No worries! Feel free to contact us on any of the following platforms below.

### Telegram

</br>

Our go-to platform would be Telegram. We are almost active everyday so we won't miss a message if you chime in. Install Telegram if you don't already have it, choose any of the groups below and scan them with a QR Code scanner to get started!

...or click on the QR code images to open the links. (QR codes are cooler, though.)

::: guide Important!
Please read the group rules, pinned messages and a few of the previous messages to see if their context is what you are looking for before 
:::

<a href="https://t.me/aurorafficial" target="_blank" rel="noopener">
  <img :src="$withBase('/assets/tg-auroraofficial-qr.png')" width="175px" />
</a>

<a href="https://t.me/aurorasupport" target="_blank" rel="noopener">
  <img :src="$withBase('/assets/tg-aurorasupport-qr.png')" width="175px" />
</a>

<a href="https://t.me/auroradroid" target="_blank" rel="noopener">
  <img :src="$withBase('/assets/tg-auroradroid-qr.png')" width="175px" />
</a>

<a href="https://t.me/AuroroaOT" target="_blank" rel="noopener">
  <img :src="$withBase('/assets/tg-auroraot-qr.png')" width="175px" />
</a>

### GitLab

</br>

All the issues and merge requsts for the code of our apps are reported on our GitLab Organization, [AuroraOSS](https://gitlab.com/AuroraOSS).


### XDA

</br>

For those who have been in the Android community for some time might know about XDA-Developers, a great place for Android enthusiasts to tinker and explore Android. Check out our XDA-Developers Forum [Thread]().

### Email

</br>

Want to write to us discreetly? Send us a [mail](mailto:auroraoss.dev@gmail.com)!

### Reddit (planned)

We're planning on starting a Reddit subreddit after we stabilized things a bit. Who wouldn't want their service to be on the world's most well-known forum service, eh?


::: danger EDIT PAGE
do a vue page for icons for each title
:::