---
title: Server status
description: Server statuses of dispensers for Aurora Store.
lang: de
meta:
  - property: og:image
    content: /icons/aurora_store.png
sidebar: true
---

# Server status
![Server status](https://telegra.ph/file/5bb8488c1b393179e3105.png)

See the current status of the token dispenser servers below:

:::: el-tabs
::: el-tab-pane label="Official servers"
<ServerTable />
:::
::::

A word from the Aurora Store team: Please do not spam our servers. If you do, we will block you _mercilessly_.
  
</br>

Recent news: We are unable to show the status of our main dispenser with Uptimerobot as of now because of our newly implemented rate limiting. However, it is running and should there be any problems with the server you can check our Telegram group chat.
  
</br>

See real-time status and history [here](https://stats.uptimerobot.com/D6QpBHB11l).

