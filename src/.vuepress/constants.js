export const GITHUB_STABLE_API_STORE = "https://api.github.com/repos/marchingon12/AuroraStore/releases/latest";
export const GITHUB_STABLE_RELEASE_STORE = "https://github.com/marchingon12/AuroraStore/releases/latest"
export const GITHUB_STABLE_API_DROID = "https://api.github.com/repos/marchingon12/AuroraDroid/releases/latest";
export const GITHUB_STABLE_RELEASE_DROID = "https://github.com/marchingon12/AuroraDroid/releases/latest"
export const GITLAB_STABLE_RELEASE = "https://gitlab.com/api/v4/projects/6922885/releases";