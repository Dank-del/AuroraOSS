---
title: Contribution
description: Find out how to contribute to AuroraOSS!
lang: en-GB
sidebar: true
---

# Contribution
Find out how to help translate or build the app and extensions.

## Code
Skilled at code? Know how to improve something or you generally want to support the creation of our apps?

### Applications
Source code of applications by AuroraOSS.
- [aurorastore](https://gitlab.com/auroraoss/aurorastore)
- [auroradroid](https://gitlab.com/auroraoss/auroradroid)
- [appwarden](https://gitlab.com/auroraoss/appwarden)
- [aurorawallpapers](https://gitlab.com/auroraoss/aurorawallpapers)
***
### Website
The repository that hosts this very website you're reading on now.
- [marchingon12/AuroraOSS](https://github.com/marchingon12/AuroraOSS) 
***
## Translation
Want to help translate the website to your language? You can easily help by utilizing a service we use called **POEditor**.

Visit our translation projects:

- [Website](https://poeditor.com/join/project/54swaCpFXJ) (coming soon).
- [Aurora Store](https://poeditor.com/join/project/54swaCpFXJ).
- [Aurora Droid](https://poeditor.com/join/project/a9lzT3YrI4).

#### Helpful links
***
- [Translators guide](https://poeditor.com/blog/translators-guide-software-localization/)

## Donation
If you can't contribute code or translations but you still wish to help, then you can choose to contribute directly to the project founder, [whyorean](https://gitlab.com/whyorean/) by using the following sources below.
***
<a href="/contribution/UPI.html">
	<img width="213" style="border:0px;width:213px;" src="/assets/upibutton.png" border="0" alt="Donate via UPI" />
</a>
<a href="http://www.paypal.me/AuroraDev" target="_blank" rel="noopener">
	<img width="213" style="border:0px;width:213px;" src="/assets/paypalbutton.png" border="0" alt="Donate with PayPal" />
</a>
<a href="https://liberapay.com/whyorean/" target="_blank" rel="noopener">
	<img width="213" style="border:0px;width:213px;" src="/assets/liberapaybutton.png" border="0" alt="Bitcoin accepted here" />
</a>

<a href="/contribution/BTC.html" >
	<img width="213" style="border:0px;width:213px;" src="/assets/bitcoinbutton.png" border="0" alt="Bitcoin accepted here" />
</a>
<a href="/contribution/BCH.html" >
	<img width="213" style="border:0px;width:213px;" src="/assets/btcashbutton.png" border="0" alt="Bitcoin accepted here" />
</a>
<a href="/contribution/ETH.html" >
	<img width="213" style="border:0px;width:213px;" src="/assets/etherumbutton.png" border="0" alt="Bitcoin accepted here" />
</a>






