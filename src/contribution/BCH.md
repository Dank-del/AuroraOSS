---
layout: DonationPage
title: Bitcoin Cash Donation
description: Donate Bitcoin Cash to AuroraOSS!
lang: en-GB
sidebar: false
---

# Bitcoin Cash

<p align="center">
    <a href="bitcoincash:qpqus3qdlz8guf476vwz0fjl8s34fseukcmrl6eknl">
        <img class="zoomable" src="https://www.bitcoinqrcodemaker.com/api/?style=bitcoincash&amp;address=qpqus3qdlz8guf476vwz0fjl8s34fseukcmrl6eknl" alt="Bitcoin Cash Wallet Address" height="250" width="250" border="0" />
    </a>
</p>

### Bitcoin Cash (BCH): 
```
qpqus3qdlz8guf476vwz0fjl8s34fseukcmrl6eknl
```
***