---
layout: DonationPage
title: UPI Donation
description: Donate via UPI to AuroraOSS!
lang: en-GB
sidebar: false
---

# UPI

<p align="center">
    <img class="zoomable" src="/assets/upiqrcode.png" alt="Bitcoin Cash Wallet Address" height="250" width="250" border="0" />
</p>
<UpiMobileButton />
