# AuroraOSS.com

An alternative static site for AuroraOSS in case our main page is down.

TODO:
- [ ] join `sideBar.js` with `sideBar[lang].js`
- [ ] add CNAME (and AlgoliaSearch maybe?) after finished
- [ ] add nightly buttons if nightly builds continue
- [ ] add more language support
- [ ] import all docs/wiki for all apps if available
- [ ] Aurora Wallpapers and App Warden mirror repos

Kudos to the team that made Tachiyomi.org for their help.